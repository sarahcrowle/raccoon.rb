# Sarah's Wiki Editing Enhancement Tools (SWEET)
## Scripts for doing various wiki things

Created for the [restless.systems](https://restless.systems/wiki) wiki.

* [raccoon.rb](raccoon.md) - For wikifying copy-pasted Discord logs
* [chairman.py](chairman.md) - For generating tables of contents for YouTube videos in WikiText

## License

All scripts in this repository are under the MIT license. Copyright 2022 Sarah Crowle. See LICENSE for more info.