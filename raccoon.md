# raccoon.rb
## A script for wikifying Discord logs

Created for the [restless.systems](https://restless.systems/wiki) wiki.

```
raccoon - convert Discord logs to wikitext
  Copyright 2022 Sarah Crowle - see LICENSE
Usage: raccoon.rb -i <input discord log> -o <output wikitext file> [options]
    -i, --log_name=LOGFILE           Input discord log (copy pasted)
    -o, --output_wikitext=OUTFILE    Output wikitext file
    -d, --today=DATE                 Date log was copied (any format)
    -u, --user_map=MAPJSON           A JSON map of username changes
```

The input format for the logs is intended to be a simple copy-paste from the Discord client or web app. If the format changes and I don't notice, please open an issue!

## Username map format

You can specify a JSON map of usernames to nicks on the command line with the `-u`/`--user_map` options. This allows you to preserve the proper users that posted the logs, as well as the funny nicknames people often have on Discord :^).

A user_map JSON file is just a single object in a file, like this:

```json
{
    "CEO of Email, Inc.": "Krabs",
    "emotional support mom friend": "sadmac356"
}
```

This will generate wikitext output that looks something like:

```
Krabs [CEO of Email, Inc.] (2022-11-10)
  some message here.
```

This parameter is optional.