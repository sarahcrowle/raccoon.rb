#!/usr/bin/env ruby

require 'Date'
require 'Optparse'
require 'JSON'

# parse options
$options = {}
option_parser = OptionParser.new do |opts|
    opts.banner = "raccoon - convert Discord logs to wikitext\n"\
        "  Copyright 2022 Sarah Crowle - see LICENSE\n"\
        "Usage: raccoon.rb -i <input discord log> -o <output wikitext file> [options]"

    opts.on("-i", "--log_name=LOGFILE", "Input discord log (copy pasted)") do |log_name|
        $options[:log_name] = log_name
    end

    opts.on("-o", "--output_wikitext=OUTFILE", "Output wikitext file") do |out_file|
        $options[:out_file] = out_file
    end

    opts.on("-d", "--today=DATE", "Date log was copied (any format)") do |date|
        $options[:today] = date
    end

    opts.on("-u", "--user_map=MAPJSON", "A JSON map of username changes") do |map_json|
        user_map = JSON.parse(File.read(map_json))
        $options[:user_map] = user_map
    end
end
option_parser.parse!

unless $options[:log_name] and $options[:out_file]
    abort option_parser.help
end

# we don't have a message yet, so blank that out.
#  also open our output file so it's ready
$current_message_group = nil
$out_f = File.open($options[:out_file], "w")

def is_header? (line)
    return !line.match(/(.*) — (.*)$/).nil?
end

def new_message_group (line)
    $current_message_group = []
    header_matches = line.match(/(.*) — (.*)$/)

    author = header_matches[1]
    time = header_matches[2]

    # normalize the time to ISO 8601 (in theory)

    # so, we need to check what "today" is, first of all. if the thread archive
    #  is really fresh, "today" will get parsed as... the day the script was run.
    #  this isn't always correct.
    if time.include? "Today at "
        # just replace "Today" with whatever the archive date is that was specified
        #  on the command line. if not, beg the user for when the archive was taken.
        unless $options[:today]
            abort "Fresh log detected. Please specify the date the log was taken"\
                " using the -d/--today option (see --help)"
        end

        time.gsub! "Today", $options[:today]
    end

    if time.include? "Yesterday at "
        unless $options[:today]
            abort "Fresh log detected. Please specify the date the log was taken"\
                " using the -d/--today option (see --help)"
        end

        yesterday = DateTime.parse($options[:today]) - 1
        time.gsub! "Yesterday", yesterday.to_s.split("T")[0]
    end

    # we can normalize the date by shoving it into a datetime, getting that as an
    #  ISO8601 string, and chopping off the time part. little cursed. whatever.
    time = DateTime.parse(time).to_s.split("T")[0]

    # we should also preserve the original usernames that posted the messages, if
    #  at all possible (when the user specifies a username map on the command line)
    user_is_nicked = false
    if $options[:user_map]
        # are they even in the provided map?
        if $options[:user_map][author]
            # if so, put in the real nick
            user_is_nicked = true
            real_author = $options[:user_map][author]
            author = "#{real_author} [#{author}]"
        end
    end

    $current_message_group.append "<div style=\"line-height: calc(100% * 0.75);\">"
    $current_message_group.append "'''#{author}''' (''#{time}'')"
    $current_message_group.append "<div style=\"margin-left: 2em; line-height: calc(100% * 1.25);\">"
end

# "what is schlorp?"
#   - Dree, 2022
def schlorp (line)
    unless line.strip.empty?
        $current_message_group.append "<nowiki>#{line}</nowiki><br>"
    end
end

def write_message_group
    $current_message_group.append "</div>\n</div>"
    $current_message_group.each do |line|
        $out_f.puts line
    end
    $out_f.puts
end

File.foreach($options[:log_name]) do |line|
    # is this line a message header?
    if is_header? line
        # do we have a current message group we'll need to output?
        if $current_message_group != nil and $current_message_group != []
            # output that to the output file first
            write_message_group
        end
        # now clear the message group, and generate the message header
        new_message_group line
    else
        # the line is just part of a message, so put it in the group
        schlorp line
    end
end

if $current_message_group != nil and $current_message_group != []
    write_message_group
end

$out_f.close