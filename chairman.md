# chairman.py
## A script for generating tables of contents for YouTube videos in WikiText

Created for the [restless.systems](https://restless.systems/wiki) wiki.

```
usage: chairman [-h] video_id timestamp_file [output_file]

Copyright 2023 Sarah Crowle

positional arguments:
  video_id
  timestamp_file
  output_file

options:
  -h, --help      show this help message and exit
```

A timestamp file looks something like this:
```
14:32 (did thing)
17:47 (did other thing)
```

Omit `output_file` to output to stdout instead.

Usage example:
```
$ python3 chairman.py tPwyWu3QYXU timestamps.log
* [https://youtube.com/watch?v=tPwyWu3QYXU?t=872s 00:14:32 - did thing]
* [https://youtube.com/watch?v=tPwyWu3QYXU?t=1067s 00:17:47 - did other thing]
```