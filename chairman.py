import argparse
import sys
import re
import datetime

parser = argparse.ArgumentParser(prog="chairman", description="Copyright 2023 Sarah Crowle")
parser.add_argument("video_id")
parser.add_argument("timestamp_file")
parser.add_argument("output_file", nargs='?', default="")
args = parser.parse_args()

if not args.output_file:
    output_file = sys.stdout
else:
    output_file = open(args.output_file, "w")

with open(args.timestamp_file, "r") as timestamp_file:
    for line_raw in timestamp_file:
        line = line_raw.strip()

        # extract our time and description
        match = re.search("(\d?\d\:\d\d) \((.*)\)", line)
        time = match.group(1)
        description = match.group(2)

        # normalize the timestamps to have an hour portion
        if len(time.split(":")) < 3:
            time = "00:" + time
        
        # convert the time into seconds using janky horrible methods
        hours, minutes, seconds = time.split(":")
        seconds = int(seconds) + (int(minutes) * 60) + (int(hours) * 60 * 60)
        
        timestamp_url = f"https://youtube.com/watch?v={args.video_id}&t={seconds}s"
        timestamp_wt = f"* [{timestamp_url} {time} - {description}]\n"
        output_file.write(timestamp_wt)

if output_file != sys.stdout:
    output_file.close()